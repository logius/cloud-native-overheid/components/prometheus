# CNO Mattermost Integration

An integration with the (CNO Mattermost role)[https://gitlab.com/logius/cloud-native-overheid/components/mattermost] is provided to facilitate a robust alerting solution out of the box. Ensure the role is configured to setup one or more alert _channels_:

```
config:
  mattermost:
    alert_webhook:
      enabled: true
      admin_team_name: "<team-name-here>"
      alert_channel_names:
      - alerts
      - alerts-test
      - <any-alert-channel-name>
```

Once these channels have been setup, Alertmanager needs to be enabled:

```
config:
  prometheus:
    alertmanager:
      enabled: true
      ingress_enabled: true
```

> Note: In a multi-cluster setup, you are responsible for propagating the secrets created by the Mattermost Role to the clusters alertmanager is running in. Handling of these secrets falls outside the scope of this role or document.


## Channel Labels

Once Alertmanager has been configured, you can start routing alerts to Mattermost. This is done by setting a special *label* on a `PrometheusRule` ruleset:

```
kind: PrometheusRule
spec:
  groups:
  - rules:
    - labels:
        cnoAlertChannel: <any-alert-channel-name>
```

Using these labels offers various benefits opposed to simply routing all alerts to one mattermost channel:
- It is possible to route alerts to dedicated teams, like `network` or `storage`
- It allows devops to debug their `PrometheusRules` in a "test" channel, where they won't disturb anyone


## Mattermost Template

When using the Mattermost integration, a template is provided to AlertManager to use when sending messages to the alert channel(s). This template supports the following labels & annotations, to be set from the `PrometheusRule` resource.


- Annotations
  - `summary` Short one-line description of the problem
  - `message` / `description` Longer description of the problem, possibly including underlying cause
  - `cluster` The cluster where the problem is occuring
  - `runbook` A link to the runbook where the user can find instructions on dealing with the issue. If no runbook is present, should link to documentation of the underlying component
- Labels
  - `severity` One of `critical`, `error`, `warning`, `info`
  
