# Custom Prometheus Ruleset

In order to trigger alerts for custom components through AlertManager, we need to define a Prometheus Ruleset. These rulesets are used by Prometheus to determine whether an alarm should be "firing". These alarms are then picked up by Alertmanager, which routes them to the proper Notifiers, such as Mattermost.

A rule conists of
- A name for the alert (`alert`)
- A PromQL query, same as are used in Grafana dashboards (`expr`)
- An amount of time for which the query must return false, before concerning the check as "firing" (`for`)
- A set of labels and annotations, which can be used to enrich the alert with relevant information, such as a Runbook which can be followed when the alert triggers.

These rules are defined in a `PrometheusRule` resource. They are grouped by file, which will be provided, through the operator, to Prometheus. 

```
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    app: prometheus-operator
    release: prometheus-operator
  name: prometheus-operator-fluentbit.rules
spec:
  groups:
  - name: fluentbit.rules
    rules:
    - alert: FluentbitRetryCountHigh
      expr: avg(sum(rate(fluentbit_output_retries_total{pod=~"fluent-fluent-bit-.*"}[5m]))) by (name) > 25
      for: 1m
      annotations:
        message: Fluentbit retry count has exceeded limit
        runbook: https://gitlab.com/logius/cloud-native-overheid/components/prometheus
      labels:
        severity: high
        namespace: cno-prometheus
```

> *Important*: In order for the Prometheus Operator to group PrometheusRules properly it is important the `namespace` label is set manually when the `PrometheusRule` is located in a different namespace than the operator itself. 